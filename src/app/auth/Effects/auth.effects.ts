import * as firebase from 'firebase';
import { User } from '../Models/user';
import { Injectable } from '@angular/core';
import { Observable, of, from } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import * as userActions from '../Actions/auth.action';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { map, switchMap, catchError } from 'rxjs/operators';

export type Action = userActions.All;

@Injectable({
  providedIn: 'root'
})
export class UserEffects {
	
	constructor(private actions: Actions, private afAuth: AngularFireAuth) {}
	
	@Effect()
	getUser: Observable<Action> = this.actions.pipe(
	  ofType(userActions.AuthActionTypes.GET_USER),
	  map((action: userActions.GetUser) => action.payload),
	  switchMap(payload => this.afAuth.authState),
	  map( authData => {
		  if (authData) {
			  const user = new User(authData.uid, authData.displayName);
			  return new userActions.Authenticated(user);
			} else {
				return new userActions.NotAuthenticated();
			}
		}),
		catchError (err => of(new userActions.AuthError()))
		);
		
	@Effect()
	login: Observable<Action> = this.actions.pipe(
		ofType(userActions.AuthActionTypes.GOOGLE_LOGIN),
		map((action: userActions.GoogleLogin) => action.payload),
		switchMap(payload => {
			return from (this.GoogleLogin());
		}),
		map(credential => {
			return new userActions.GetUser();
		}),
		catchError(err => {
			return of(new userActions.AuthError({error: err.message}));
		})
	);
	
	private GoogleLogin(): Promise<firebase.auth.UserCredential> {
		const provider = new firebase.auth.GoogleAuthProvider();
		return this.afAuth.auth.signInWithPopup(provider);
	}
	
	@Effect()
	Logout: Observable<Action> = this.actions.pipe(
		ofType(userActions.AuthActionTypes.LOGOUT),
		map((action: userActions.Logout) => action.payload),
		switchMap(payload => {
			return of(this.afAuth.auth.signOut());
		}),
		map( authData => {
			return new userActions.NotAuthenticated();
		}),
		catchError(err => {
			console.log('logout', err);
			return of(new userActions.AuthError({err: err.message}));
		})
	);
}