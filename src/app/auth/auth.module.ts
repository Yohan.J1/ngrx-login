import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import * as fromAuth from './Reducers/auth.reducer';
import { UserEffects } from './Effects/auth.effects';
import { LoginComponent } from './login/login.component';
import { AuthRoutingModule } from './auth-routing.module';

@NgModule({
	imports: [
		CommonModule,
		AuthRoutingModule,
		FormsModule,
		StoreModule.forFeature('auth', fromAuth.userReducer),
		EffectsModule.forFeature([UserEffects])
	],
	declarations: [LoginComponent]
})
export class AuthModule { }