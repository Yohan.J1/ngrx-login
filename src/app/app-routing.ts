import { Routes } from '@angular/router';

export const appRoutes: Routes = [
	{ path: '', redirectTo: '/login', pathMatch: 'full' },
	{ path: 'login', loadChildren:  './auth/auth.module#AuthModule' },
	{ path: '**', redirectTo: '/login' }
];
